#!/bin/bash

echo "var models = [" > model_lst.js
find ../../../models -name "*.glb"|while read line;do 
  echo "\"$line\",";
done >> model_lst.js

echo '""' >> model_lst.js
echo "]" >> model_lst.js
